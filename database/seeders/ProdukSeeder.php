<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'nama_produk' => 'Iphone X',
            'harga'  => '21000000',
            'stok'  =>'10' ,
            
        ]);
        Product::create([
            'nama_produk' => 'Samsung X',
            'harga'  => '3000000',
            'stok'  =>'10' ,
            
        ]);
        Product::create([
            'nama_produk' => 'Vivo X',
            'harga'  => '11000000',
            'stok'  =>'10' ,
            
        ]);
        
    }
}
