<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post');
Route::post('logout', [AuthController::class, 'logout'])->name('logout');

Route::prefix('/admin')->middleware(['auth'])->group(function () {
});

    Route::resource('mahasiswa', MahasiswaController::class)->names([
        'index' => 'mahasiswa.index',
        'create' => 'mahasiswa.create',
        'store' => 'mahasiswa.store',
        'show' => 'mahasiswa.show',
        'edit' => 'mahasiswa.edit',
        'update' => 'mahasiswa.update',
        'destroy' => 'mahasiswa.destroy',
    ]);
    Route::resource('sales', SalesController::class)->names([
        'index' => 'sales.index',
        'create' => 'sales.create',
        'store' => 'sales.store',
        'show' => 'sales.show',
        'edit' => 'sales.edit',
        'update' => 'sales.update',
        'destroy' => 'sales.destroy',
    ]);


// Menyesuaikan dengan tindakan CRUD lainnya jika diperlukan
