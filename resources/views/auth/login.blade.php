<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="{{ asset('images/logo/logo-kab.png') }}" type="image/x-icon" />
  <title>Portal Admin Desa | Login</title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/lineicons.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/materialdesignicons.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
</head>
<body>
  <section class="signin-section">
    <div class="container-fluid">
      <div class="row g-0 auth-row">
        <div class="col-lg-6">
          <div class="auth-cover-wrapper bg-success-100">
            <div class="auth-cover">
              <div class="title text-center">
                <h1 class="text-success mb-10">Selamat Datang</h1>
                <p class="text-medium">
                  Silahkan Login untuk Portal Desa Dayeuhkolot
                </p>
              </div>
              <div class="cover-image">
                <img src="{{ asset('assets/images/auth/signin-image.svg') }}" alt="" />
              </div>
              <div class="shape-image">
                <img src="{{ asset('assets/images/auth/shape.svg') }}" alt="" />
              </div>
            </div>
          </div>
        </div>
        <!-- end col -->
        <div class="col-lg-6">
          <div class="signin-wrapper">
            <div class="form-wrapper">
              <div class="text-center w-100">
                <img class="text-center" src="{{ asset('images/logo/DesaDayeuhkolot.png') }}" width="200px" height="50px" alt="logo">
              </div>
              <br> 
              <br>
              <h6 class="mb-15 mt-20">Portal Desa Dayeuhkolot</h6>
              <p class="text-sm mb-25">
                Silahkan Login untuk mengakses Portal Desa Dayeuhkolot
              </p>
              <form action="{{ route('login.post') }}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-12">
                    <div class="input-style-1">
                      <label>Username</label>
                      <input type="text" placeholder="Username" id="username" name="username" required autofocus/>
                      @if ($errors->has('username'))
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                      @endif
                    </div>
                  </div>
                  <!-- end col -->
                  <div class="col-12">
                    <div class="input-style-1">
                      <label>Password</label>
                      <input type="password" placeholder="Password" id="password" name="password" required autofocus/>
                      @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif
                    </div>
                  </div>
                  <!-- end col -->
                  <div class="col-12">
                    <div class="button-group d-flex justify-content-center flex-wrap">
                      <button class="main-btn success-btn btn-hover w-100 text-center" type="submit">Sign In</button>
                    </div>
                  </div>
                </div>
                <!-- end row -->
              </form>
              <div class="singin-option pt-40">
                <div class="button-group pt-40 pb-40 d-flex justify-content-center flex-wrap">
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end col -->
      </div>
      <!-- end row -->
    </div>
  </section>
  @include('sweetalert::alert')
</body>
</html>
