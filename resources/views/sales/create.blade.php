@extends('template.template')

@section('title', 'Portal Admin Desa | Dashboard')

@section('content')
<section class="section">
    <div class="container-fluid">
        <div class="title-wrapper pt-30">
            <h2 class="mb-20">Form Data Penjualan</h2>
            <form action="{{ route('sales.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="form-elements-wrapper">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- input style start -->
                                <div class="card-style mt-2 mb-30">
                                    <div class="select-style-2">
                                        <label for="id_produk">Produk</label>
                                        <select name="id_produk" id="id_produk" class="form-control">
                                            <option value="">Pilih Produk</option>
                                            @foreach ($products as $product)
                                                <option value="{{ $product->id }}" data-harga="{{ $product->harga }}"
                                                    data-total="{{ $product->harga }}">{{ $product->nama_produk }}</option>
                                            @endforeach
                                        </select>
                                        @error('id_produk')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="input-style-1">
                                        <label for="nama_customer">Nama Customer</label>
                                        <input type="text" name="nama_customer" id="nama_customer"
                                            class="form-control">
                                        @error('nama_customer')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="input-style-1">
                                        <label for="harga">Harga</label>
                                        <input type="text" name="harga" id="harga" class="form-control" readonly>
                                    </div>

                                    

                                    <div class="input-style-1">
                                        <label for="qty">Qty:</label>
                                        <input type="number" name="qty" id="qty" class="form-control" min="1">
                                        @error('qty')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="input-style-1">
                                        <label for="total">Total</label>
                                        <input type="text" name="total" id="total" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="modal-footer">
                                        <a href="{{ route('sales.index') }}"
                                            class='btn light-btn square-btn btn-hover btn-sm'
                                            style="padding: 8px 14px">Tutup</a>
                                        <button type='submit' id='menu-toggle' id="submit-data"
                                            class='btn success-btn square-btn btn-hover btn-sm'
                                            style="padding: 8px 14px" value="Upload">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script>
    // Mengatur event listener saat memilih produk
    document.getElementById('id_produk').addEventListener('change', function () {
        var selectedOption = this.options[this.selectedIndex];
        var hargaInput = document.getElementById('harga');
        hargaInput.value = selectedOption.getAttribute('data-harga');
        calculateTotal();
    });

    // Mengatur event listener saat mengubah kuantitas
    document.getElementById('qty').addEventListener('input', function () {
        calculateTotal();
    });

    // Fungsi untuk menghitung total harga
    function calculateTotal() {
        var hargaInput = document.getElementById('harga');
        var qtyInput = document.getElementById('qty');
        var totalInput = document.getElementById('total');

        var harga = parseFloat(hargaInput.value);
        var qty = parseInt(qtyInput.value);
        var total = harga * qty;

        totalInput.value = total.toFixed(2);
    }
</script>
@endsection
