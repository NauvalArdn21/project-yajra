@extends('template.template')
@section('title','Portal Admin Desa | Dashboard')
{{-- kalau ada css tambahan selain dari template.blade --}}
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
    <link href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">@endpush

@section('content')
<section class="section">
    <div class="container-fluid">
      <!-- ========== title-wrapper start ========== -->
        <div class="title-wrapper pt-30">
            <div class="row align-items-center">
            <div class="col-md-6">
            {{-- @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif --}}
                <div class="titlemb-30">
                <h2>Data User</h2>
                </div>
            </div>
            <!-- end col -->
            <div class="col-md-6">
                <div class="breadcrumb-wrapper mb-30">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#0">User</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Page
                    </li>
                    </ol>
                </nav>
                </div>
            </div>
            <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    <a href="{{route('mahasiswa.create')}}" class="btn btn-sm rounded-full btn-success mb-3">
                            <i class="lni lni-circle-plus"></i> Tambah User
                        </a>
                        
                       
                        <div class="table-wrapper table-responsive">
                            <table class="table" id="mahasiswa">
                                <thead>
                                <tr>
                                    <th>no</th>
                                <th>ID</th>
                         <th>Nama</th>
                         <th>Email</th>
                         <th>Email</th>
                         <th>Aksi</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>
<div id="showDelete" class="modal fade bd-example-modal-mb" tabindex="-1" role="dialog" aria-labelledby="add-categori" aria-hidden="true">
    <div class="modal-dialog modal-mb modal-dialog-centered">
      <div class="modal-content card-style ">
            <div class="modal-header px-0">
                <h5 class="text-bold" id="exampleModalLabel">Hapus Data</h5>
                <button type="button" class="border-0 bg-transparent h1" data-bs-dismiss="modal" aria-label="Close">
                  <i class="lni lni-cross-circle"></i>
                </button>
            </div>
          <div class="modal-body px-0">
            <form action="" method="post" id="delete-form">
                @method('DELETE')
                @csrf

                <p class="mb-40">Apakah anda yakin ingin menghapus data ini?</p>
                <div class="action d-flex flex-wrap justify-content-end">
                    <button type="button" data-bs-dismiss="modal" class='main-btn success-btn-outline btn-hover m-1'>Cancel</button>
                    <button type='submit' class='main-btn danger-btn btn-hover m-1'>Submit</button>
                </div>
            </form>
          </div>
      </div>
    </div>
</div>

    @endsection
    @push('js')
<!-- <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form> -->
  @include('sweetalert::alert')
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            $('#showDelete').modal('show');
            $("#delete-form").attr('action', $(el).attr('href'));
        }
        $(document).ready(function() {
            $('#mahasiswa').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('mahasiswa.index') }}",
                columns: [
                    {data: 'id', name: 'id', render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }},
                    {data: 'NIM', name: 'NIM'},
                    {data: 'nama', name: 'nama'},
                    {data: 'jurusan', name: 'jurusan'},
                    {data: 'prodi', name: 'prodi'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

@endpush