<?php

namespace App\Http\Controllers;

use App\Models\Sales;
use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert;

class SalesController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = sales::all();
            return DataTables::of($data)
            
            ->addColumn('action', function ($row) {
                $html = '<a href=' . route('sales.edit', $row) . ' style="font-size:20px" class="text-warning mr-10"><i class="lni lni-pencil-alt"></i></a>';

                $html .= '<a href=' . route('sales.destroy', $row) .   ' style="font-size:20px" class="text-danger" onclick="notificationBeforeDelete(event, this)"><i class="lni lni-trash-can"></i></a>';
                return $html;

            })

            ->addColumn('nama_produk', function ($row) {
                return  ($row->product->nama_produk);
             })
            
            ->rawColumns(['nama_produk','action'])

            ->make(true);
        }
        return view('sales.index');
    }
    

    public function create()
    {
        $products = Product::all();
        return view('sales.create', compact('products'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id_produk' => 'required|exists:products,id',
            'nama_customer' => 'required|string|max:255',
            'qty' => 'required|integer|min:1',
        ]);

        
        $product = Product::findOrFail($validatedData['id_produk']);
        $total = $product->harga * $validatedData['qty'];

        $product->stok -= $validatedData['qty'];
        $product->save();
        
        $sales = new Sales();
        $sales->id_produk = $validatedData['id_produk'];
        $sales->nama_customer = $validatedData['nama_customer'];
        $sales->harga = $product->harga;
        $sales->qty = $validatedData['qty'];
        $sales->total = $total;
        $sales->save();

        return redirect()->route('sales.index')->with('success', 'Penjualan berhasil ditambahkan!');
    }


    public function destroy($id) 
    {
            $sales = Sales::findOrFail($id);
            $sales->delete();
            Alert::success('Sukses', 'Data sales berhasil dihapus.');
            return redirect()->route('sales.index');
    }

    public function edit($id)
    {
        $products = Product::all();

        $sales = Sales::find($id);
        return view('sales.edit', compact('sales','products'));
    }

    public function update(Request $request, $id)
    {
       
        $request->validate([
            'nama_customer' => 'required',       
            'produk' => 'required',
            'qty' => 'required',
        ]);
        $sales = Sales::find($id);
        //Proses Upload Foto
        $sales->NIM = $request->NIM;
        $sales->nama = $request->nama;
        $sales->jurusan = $request->jurusan;
        $sales->prodi = $request->prodi;
        $sales->save();
        return redirect()->route('sales.index')
            ->withSuccess('Berhasil Mengubah Data sales');
    }

}
