@extends('template.template')

@section('title','Portal Admin Desa | Dashboard')

{{-- kalau ada css tambahan selain dari template.blade --}}
{{-- @push('css')
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush --}}

@section('content')
<section class="section">
<div class="container-fluid">   
<div class="title-wrapper pt-30">
    <h2 class="mb-20">Edit Data Mahasiswa</h2>
      <form action="{{route('mahasiswa.store')}}" method="post">
    @csrf
    <div class="row">
    <div class="form-elements-wrapper">
            <div class="row">
              <div class="col-lg-12">
                <!-- input style start -->
                <div class="card-style mt-2 mb-30">
                
                  <div class="input-style-1">
                        <label for="exampleInputName">NIM </label>
                        <input type="Number" class="form-control @error('NIM') is-invalid @enderror" id="exampleInputName" placeholder="NIM" name="NIM" value="{{old('NIM')}}">
                        @error('NIM') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="input-style-2">
                        <label for="exampleInputName">nama</label>
                        <input type="Text" class="form-control @error('nama') is-invalid @enderror" id="exampleInputName" placeholder="nama" name="nama" value="{{old('nama')}}">
                        @error('nama') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="input-style-2">
                        <label for="exampleInputName">jurusan</label>
                        <input type="Text" class="form-control @error('jurusan') is-invalid @enderror" id="exampleInputName" placeholder="jurusan" name="jurusan" value="{{old('jurusan')}}">
                        @error('jurusan') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="input-style-2">
                        <label for="exampleInputName">prodi</label>
                        <input type="Text" class="form-control @error('prodi') is-invalid @enderror" id="exampleInputName" placeholder="prodi" name="prodi" value="{{old('prodi')}}">
                        @error('prodi') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    
                </div>
            </div>
                <div class="row">
                        <div class="col-12">
                          <div class="card">
                                <div class="modal-footer">
                                    <a href="{{route('mahasiswa.index')}}" class='btn light-btn square-btn btn-hover btn-sm' style="padding: 8px 14px">Tutup</a>
                                    <button type='submit' id='menu-toggle' id="submit-data" class='btn success-btn square-btn btn-hover btn-sm' style="padding: 8px 14px" value="Upload">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection

