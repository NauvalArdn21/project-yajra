<!-- ======== sidebar-nav start =========== -->
<aside class="sidebar-nav-wrapper">
    <div class="navbar-logo" style="text-align: left">
      <a href="index.html">
      <img src="{{asset('images/gambar.png')}}" width="96px" height="66px" alt="logo" />
      </a>
    </div>
    <nav class="sidebar-nav">
      <ul>
        <li class="nav-item {{ Request::routeIs('mahasiswa.index') ? 'active' : '' }}">
            <a href="{{route('mahasiswa.index')}}">
              <span class="icon">
                <svg width="22" height="22" viewBox="0 0 22 22">
                  <path
                    d="M17.4167 4.58333V6.41667H13.75V4.58333H17.4167ZM8.25 4.58333V10.0833H4.58333V4.58333H8.25ZM17.4167 11.9167V17.4167H13.75V11.9167H17.4167ZM8.25 15.5833V17.4167H4.58333V15.5833H8.25ZM19.25 2.75H11.9167V8.25H19.25V2.75ZM10.0833 2.75H2.75V11.9167H10.0833V2.75ZM19.25 10.0833H11.9167V19.25H19.25V10.0833ZM10.0833 13.75H2.75V19.25H10.0833V13.75Z"
                  />
                </svg>
              </span>
              <span class="text">Dashboard</span>
            </a>
        </li>
        <li class="nav-item {{ Request::routeIs('sales.index') ? 'active' : '' }}">
            <a href="{{route('sales.index')}}">
            <span class="icon">
              <svg width="22" height="22" viewBox="0 0 22 22">
                <path fill="currentColor" d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
              </svg>
            </span>
            <span class="text">Data Penjualan</span>
          </a>
        </li>
        
  </aside>
  <div class="overlay"></div>
  <!-- ======== sidebar-nav end =========== -->
