@extends('template.template')

@section('title','Portal Admin Desa | Dashboard')

{{-- kalau ada css tambahan selain dari template.blade --}}
{{-- @push('css')
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush --}}

@section('content')
<section class="section">
<div class="container-fluid">   
<div class="title-wrapper pt-30">
    <h2 class="mb-20">Edit Data Penjualan</h2>
      <form action="{{route('sales.store')}}" method="post">
    @csrf
    <div class="row">
    <div class="form-elements-wrapper">
            <div class="row">
              <div class="col-lg-12">
                <!-- input style start -->
                <div class="card-style mt-2 mb-30">
                
                <div class="input-style-1">
                <label for="id_produk">Produk</label>
                <select name="id_produk" id="id_produk" class="form-control">
                    <option value="">Pilih Produk</option>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}" data-harga="{{ $product->harga }}">{{ $product->nama_produk }}</option>
                    @endforeach
                </select>
                @error('id_produk')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="input-style-1">
                <label for="nama_customer">Nama Customer</label>
                <input type="text" name="nama_customer" id="nama_customer" class="form-control">
                @error('nama_customer')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="input-style-1">
                <label for="qty">Qty</label>
                <input type="number" name="qty" id="qty" class="form-control" min="1">
                @error('qty')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

                    
             
            </div>
                <div class="row">
                        <div class="col-12">
                          <div class="card">
                                <div class="modal-footer">
                                    <a href="{{route('sales.index')}}" class='btn light-btn square-btn btn-hover btn-sm' style="padding: 8px 14px">Tutup</a>
                                    <button type='submit' id='menu-toggle' id="submit-data" class='btn success-btn square-btn btn-hover btn-sm' style="padding: 8px 14px" value="Upload">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<script>
        // Mengatur event listener saat memilih produk
        document.getElementById('id_produk').addEventListener('change', function() {
            var selectedOption = this.options[this.selectedIndex];
            var hargaInput = document.getElementById('harga');
            hargaInput.value = selectedOption.getAttribute('data-harga');
        });
    </script>
@endsection

