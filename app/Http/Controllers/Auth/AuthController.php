<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('mahasiswa.index')->with('toast_success', 'You have successfully logged in');
        } else {
            return redirect()->route('login')->with('toast_error', 'Oops! You have entered invalid credentials');
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();

        return redirect()->route('login')->with('toast_success', 'You have successfully logged out');
    }
}
