<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert;



class MahasiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Mahasiswa::all();
            return DataTables::of($data)
            ->addColumn('action', function ($row) {
                $html = '<a href=' . route('mahasiswa.edit', $row) . ' style="font-size:20px" class="text-warning mr-10"><i class="lni lni-pencil-alt"></i></a>';

                $html .= '<a href=' . route('mahasiswa.destroy', $row) .   ' style="font-size:20px" class="text-danger" onclick="notificationBeforeDelete(event, this)"><i class="lni lni-trash-can"></i></a>';
                return $html;
            })
            ->make(true);
        }
        return view('mahasiswa.index');
    }

    public function create()
    {
       
        return view('mahasiswa.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'NIM' => 'required',       
            'nama' => 'required',
            'jurusan' => 'required',
            'prodi' => 'required',
            
        ]);


            $data = $request->all();
            $mahasiswa = Mahasiswa::create($data);
            return redirect()->route('mahasiswa.index')
            ->withSuccess('Berhasil Menambah Data Mahasiswa');
        
    }

    public function destroy($NIM) 
    {
            $mahasiswa = Mahasiswa::findOrFail($NIM);
            $mahasiswa->delete();
            Alert::success('Sukses', 'Data mahasiswa berhasil dihapus.');
            return redirect()->route('mahasiswa.index');
    }

    public function edit($NIM)
    {
        $mahasiswa = Mahasiswa::find($NIM);
        return view('mahasiswa.edit', compact('mahasiswa'));
    }

    public function update(Request $request, $NIM)
    {
       
        $request->validate([
            'NIM' => 'required',       
            'nama' => 'required',
            'jurusan' => 'required',
            'prodi' => 'required',
        ]);
        $mahasiswa = Mahasiswa::find($NIM);
        //Proses Upload Foto
        $mahasiswa->NIM = $request->NIM;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->jurusan = $request->jurusan;
        $mahasiswa->prodi = $request->prodi;
        $mahasiswa->save();
        return redirect()->route('mahasiswa.index')
            ->withSuccess('Berhasil Mengubah Data mahasiswa');
    }

}
